---
home: Help Center 
lang: en-AU
heroText: Help Center 
heroImage: /hero.svg
actionText: Get Started →
actionLink: /guide/get_started/
features:
- title: Getting Started
  details: Explore the articles about Account, User, Billing. Design and build a chatbot to automate tasks and turn conversion. Build stronger relationships with customers by delivering targeted content and anticipating their reactions.
- title: Flow Builder
  details: The power, flexibility and ease of use of the visual flow builder enables rapid intelligent chatbot development for everyone. No coding or technical skills required.
- title: Integration
  details: Extend your chatbot functionality to create a truly personalized experience, UChat provides a rich set of actions that easily integrates with external services such as Slack, Google Spreadsheet, SendGrid, Zapier, and countless others.
footer: Licensed | Copyright © 2020-present 
---
### As Easy as 1, 2, 3
<div class="features">
<div class="feature">
<h2>
1. Create
</h2>
<p>
You can create an automated(bot) or Human (Human Hybrid) chatbot. No coding or technical skills required
</p>
</div>
<div class="feature">
<h2>
2. Build
</h2>
<p>
Design conversations to utilise "Simple" or "Multiple Choice" or something more complex such as action buttons, translation, collect payments, send receipts and more. No coding or technical skills required
</p>
</div>
<div class="feature">
<h2>
3. Publish
</h2>
<p>
Design conversations to utilise "Simple" or "Multiple Choice" or something more complex such as action buttons, translation, collect payments, send receipts and more. No coding or technical skills required
</p>
</div>
</div>
<script>
document.getElementsByTagName("h2")[0].onclick = function () {
        location.href = "/guide/get_started";}
document.getElementsByTagName("h2")[0].style.cursor = "pointer";
document.getElementsByTagName("h2")[0 ].style.color = "green";
document.getElementsByTagName("h2")[1].onclick = function () {
        location.href = "/guide/flow_builder";}
document.getElementsByTagName("h2")[1].style.cursor = "pointer";
document.getElementsByTagName("h2")[1].style.color = "green";
document.getElementsByTagName("h2")[2].onclick = function () {
        location.href = "https://uchat.com.au";}
document.getElementsByTagName("h2")[2].style.cursor = "pointer";
document.getElementsByTagName("h2")[2].style.color = "green";


</script>
---
prev: /guide/
next: /guide/account_user/
---
# Getting Started
---
## Registration and Authorization
#### Registration
To begin the process of creating your own chatbot, you'll need to register on UChat's website, it only takes a moment. The platform is built with robust administrative features and uses high-grade security that complies with all regulatory mandates, so data is secure and safe.

First, you need to enter your account name.

Next, you have to enter your name, a valid email address, and you must create a password.

#### Login

Next time when you need to sign in you must enter your email and password on the authorization page and press “Sign In”. If you want to remain logged in, tick “Remember me”.
Also, you can log in via a Facebook or Google account.
Once you’re authorized you will get to the dashboard.

#### Forgot your password

To restore your password click “Forgot your password?” on the “Login” page. You will get to the recovery page where you need to enter your email address and press “Reset your password”. Next, you will see a message “To proceed, please check your email”, and an email with a restore link will be sent to the email address that you gave.

Once you open the link you will get to the reset password window. You have to enter a new password in the fields "Password" and "Confirm password" and press Reset to proceed.

---
## Create your first flow
---
When you first log in, you will be taken to the Account Dashboard, which you will use to see all your chatbot analytics and flows.

Select All Flows from the left sidebar, then click the Create Flow button under different flow type box.

On this page, you can look through existing flow templates, search them or filter by categories.

To create a new flow, you can select Blank Template or an existing template.

::: tip
There are different templates will be displayed based on the selected flow type.
:::

You can enter a Flow Name and Description for your flow, to complete the process of creating a chatbot press the button "Create Flow" in the top right corner. You will see the notification: “Great Work, You have created a new flow, start working by adding interactions”.

Next, comes the real fun, you flow will communicate with users through the Flow steps.

---
module.exports = {
  // 其它配置
  themeConfig: {
    nav: [
      { text: 'Guide', link: '/guide/' },
      { text: 'Flow Builder', link: '/FB/' },
      { text: 'UChat Home', link: 'https://www.uchat.com.au/' }
    ],
    sidebar: [
      {
        title: '/guide/',   // 必要的
        path: '/guide/',      // 可选的, 标题的跳转链接，应为绝对路径且必须存在
        collapsable: false, // 可选的, 默认值是 true,
        sidebarDepth: 1,    // 可选的, 默认值是 1
        children: [
            {
              title: 'Introduction',
              path: '/guide/',  
              collapsable: false,
              children: [ /* ... */ ]
      }
    ]
  }
}
---
prev: /guide/account_user/
next: /guide/flow_builder/
---
#  Billing

Click on the name of your Account in the top left corner and select your 'Account Name' from the menu. Then, you will find the "Account Billing" section in the left sidebar.

You can manage your subscriptions, add and change your payment method, preview and download invoices.

## Add a payment method

To add a payment method, just click on the "Payment method" from the left sidebar.
:::  warning NOTE:
it is currently only possible to add a credit card as a saved payment method.
:::
---
##  How to change the default payment method

To change your default payment method, you can click on the "Payment method" from the left sidebar. Fill in your credit card detail and other information in the form, and then click on the button "Update" to update your payment method.

---
## Payment methods we support

#### Credit card

We support VISA and MasterCard payments in all countries.

## Invoices

Click on the name of your Account in the top left corner and select your 'Account Name' from the menu. Then, you will find the "Invoices" link from the "Account Billing" section in the left sidebar. Here you can see your invoices and download invoices.

You can add Extra Billing Information to your Invoices.

## Subscriptions
Click on the name of your Account in the top left corner and select your 'Account Name' from the menu. Then, you will find the "Subscriptions" link from the "Account Billing" section in the left sidebar. Here you can see an overview of your active subscription. You can also change or cancel your subscription.


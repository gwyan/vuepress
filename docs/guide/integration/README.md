# Accounts & Users

## Accounts

Account is where you can control your flows, templates, integrations, billings, manage your contacts and more. You can also monitor your insights, regarding anything tied to your Account.

Account will be created for you automatically once you sign up for a UChat account. As a main point of contact, you can then invite other people to your company (via email) to become users in the Account.

---
##  Users

Users are all the people that have access to the Account. Only one user is the owner of the Account, and only the owner can:

* Add and delete other users.
* Manage the Integrations, Billing

As an Account owner, you can invite other people to your company (via email) to become users in your Account. The people you invite will have to create a UChat account and each user will have their own login details. They will be added as members, and will have the same rights as you (the owner) except they cannot:

* Add and delete other users.
* Manage the Integrations, Billing

---
## How to add, edit & delete users

Users are all the people that have access to the Account. One user is the owner of the Account, and only the owner can add and delete other users. Each user has his or her own login details.

To view and delete users, click on the Account name in the upper left corner and click on 'Account Name' in the drop-down menu. Then click on "Members" on the left sidebar.

When you add a new user, an invitation will be sent to the email address of the new user. This invitation will contain a link to set up a new password. Once set up, the users can now also log into the Account.

## Add & switch between Accounts
#### Switch Account
You can easily switch between Accounts by clicking on your Account name in the upper left corner. If you click on your account name, a drop-down menu will appear. You will see an overview of your different Accounts and where you can easily switch between them.
#### Create Account
To create new Account, you can click on your Account name in the upper left corner. If you click on your account name, a drop-down menu will appear, then click on 'Create Account'.

**Please note** that it can happen that when you log in, the system accidentally logs you into the wrong Account so that it could seem like your flows are missing or different. Most of the time, this can be solved by switching to another Account.


# Introduction
---
<br>
<br>

:clap: Creating your first Chatbot on the UChat platform.
::: tip Note
This is a walkthrough to see the basics of the UChat platform in action. Read the Complete Guide to learn about the platform in more detail.
:::
module.exports = {
  title: 'Help Center',
  head: [
    ['link', { rel: 'logo', href: '/logo1.png' }]
  ],
  themeConfig: {
    plugins: ['@vuepress/medium-zoom'],
    logo: '/logo1.png',  
    nav: [
      { text: 'Guide', link: '/guide/' },
      { text: 'Flow Builder', link: '/guide/flow_builder/' },
      { text: 'UChat Home', link: 'https://www.uchat.com.au/' }
    ],
    sidebar: [
      {
        title: 'guide',  
        path: '/guide/',     
        collapsable: false, 
        sidebarDepth: 1,    
        children: [
            {
              title: 'Introduction',
              path: '/guide/',  
              collapsable: false,
              children: [ /* ... */ ]
            },
            {
              title: 'Get Started',
              path: '/guide/get_started/',  
              collapsable: false,
              children: [ /* ... */ ]
            },
            {
              title: 'Account & Users',
              path: '/guide/account_user/',  
              collapsable: false,
              children: [ /* ... */ ]
            },
            {
              title: 'Billing',
              path: '/guide/billing/',  
              collapsable: false,
              children: [ /* ... */ ]
            }
        ]
      }
    ]
  }
}

